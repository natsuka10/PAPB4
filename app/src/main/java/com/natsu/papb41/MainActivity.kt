package com.natsu.papb41

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val nameInput = findViewById<EditText>(R.id.tfName)
        val nimInput = findViewById<EditText>(R.id.tfNim)
        val addBtn = findViewById<Button>(R.id.button)
        //Recycle View
        val listMahasiswa = mutableListOf<Item>(
            Item(
                R.drawable.profile1,
                "Farid Wajdi",
                "215150400111017"
            ),
            Item(
                R.drawable.profile1,
                "Ruslan Renada",
                "215150400111018"
            ),
        )
        addBtn.setOnClickListener {
            val nama = nameInput.text.toString()
            val nim = nimInput.text.toString()
            val newItem = Item(
                R.drawable.profile1,
                nama,
                nim)

            listMahasiswa.add(newItem)

            //to reset text field after submit
            nameInput.text.clear()
            nimInput.text.clear()

            //Hide softkeyboard after submit
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(nameInput!!.windowToken, 0)

        }

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerHero)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = AdapterItem(this, listMahasiswa){
        }
    }
}