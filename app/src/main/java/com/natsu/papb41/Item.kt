package com.natsu.papb41

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Item  (
    val imageHero: Int,
    val nameHeroes: String,
    val subnameHeroes: String
) : Parcelable


