package com.natsu.papb41

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class AdapterItem(private val context: Context, private val items: List<Item>, val listener: (Item) ->Unit)
    : RecyclerView.Adapter<AdapterItem.ItemsViewHolder>() {

    class ItemsViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val imageHero = view.findViewById<ImageView>(R.id.img_item)
        val nameHeroes = view.findViewById<TextView>(R.id.tv_item_name)
        val subnameHeroes = view.findViewById<TextView>(R.id.tv_item_subname)

        fun bindingview(items: Item, listener: (Item) -> Unit){
            imageHero.setImageResource(items.imageHero)
            nameHeroes.text = items.nameHeroes
            subnameHeroes.text = items.subnameHeroes
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemsViewHolder {

        return ItemsViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_list, parent,false)
        )
    }

    override fun getItemCount(): Int = items.size


    override fun onBindViewHolder(holder: ItemsViewHolder, position: Int) {
        holder.bindingview(items[position], listener)
    }
}
